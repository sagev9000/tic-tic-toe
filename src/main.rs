extern crate timer;	//
extern crate chrono;//
extern crate rand;	// For AI randomness

use std::thread;	// For sleeping
use std::io;		// For terminal interactions
use std::io::Write;	//
use std::env;		// For argument processing
use rand::Rng;		// For AI randomness

struct FirstBot{play_char: char}
struct OddFirstBot{play_char: char}
struct RandBot{play_char: char}
struct HumanBot{play_char: char}
struct LearnBot{
    play_char: char,
	// 2D array for tracking changing odds in the learn_mark() AI
	// probarr[turn number][move location] = odds of picking that location on that turn
	probarr: [[i32; 9]; 9],
	probarr_old: [[i32; 9]; 9],
	history: [i8; 9],
    wins: i64,
    losses: i64,
    wins_old: i64,
    losses_old: i64,
    prob_iter: i32,
}

trait Bot {
    fn bot_move(&mut self, mut _board: &mut [char], _turn: i8) {}
    fn name(&self) -> &str { "Bot" }
    fn c(&self) -> char { ' ' }
    fn learn(&mut self, _board: &[char]) {}
    //fn new(play_char: char) -> Self;
}

// Maybe implement threading, to display games at a human-viewable rate
impl Bot for LearnBot {
    fn learn(&mut self, _board: &[char]) {
        if game_over(&_board) == ' ' {return;}

        let i_won = game_over(&_board) == self.play_char;

        if i_won {
            self.wins = self.wins + 1;
        } else {
            self.losses = self.losses + 1;
        }

        let inv = match i_won {
            x if x => 1,
            _ => -1,
        };

        // Max/min probarr value
        let comp = match i_won {
            x if x => 1000000,
            _ => 2000,
        };

        /*
        let shift = match i_won {
            x if x => 1000,
            _ => 1000,
        };
        */

        for x in 0..8 {
            // If that turn was the learner's
            if self.history[x] >= 0 {
                let mut shift: f32;
                if i_won {
                    shift = self.probarr[x][self.history[x] as usize] as f32 * 1.01;
                    if shift > 1000000.0 { shift = self.probarr[x][self.history[x] as usize] as f32; }
                } else {
                    shift = self.probarr[x][self.history[x] as usize] as f32 * 0.999;
                    if shift < 1.0 { shift = 1.0; }
                }

                self.probarr[x][self.history[x] as usize] = shift as i32;

                /*
                // And the new probval is in range
                if self.probarr[x][self.history[x] as usize]*inv < comp*inv{ 

                    // Boost the odds of picking that move by ~1%
                    let oddshift = inv*(self.probarr[x][self.history[x] as usize] /shift);
                    self.probarr[x][self.history[x] as usize] += oddshift;
                } else {
                    //if !i_won {
                        //self.probarr[x][self.history[x] as usize] = 100;
                    //}
                }
                */
            }
        }
        
        let prob_position = match self.play_char {
            'X' => 8,
            _ => 19,
        };

        // Below the board,
        if i_won {
            print!("{}[{};0HProbability Array {}-{}", 
                   27 as char, prob_position, self.play_char, self.prob_iter);

            for n in 0..9 { //< Output each row
                print!("\nTurn { }:\t", n);
                for o in 0..9 { //< Output each column
                    print!("{}\t", self.probarr[n][o]/1000) 
                }
            }
            println!("Total: {} \tOld: {}", self.wins/(self.losses+1), (self.wins_old/(self.losses_old+1)));
        }
        
        // TODO:
        //  - Save a copy of probarr when the win ratio is high, 
        //    and restore it if the ratio drops.
        //  - Adjust shift values dynamically
        if (self.wins + self.losses) % 500 == 499 {
            if (self.wins/(self.losses+1)) < (self.wins_old/(self.losses_old+1)) {
                self.wins = self.wins_old;
                self.losses = self.losses_old;
                self.probarr = self.probarr_old;
                self.prob_iter = self.prob_iter + 1;
            } else {
                self.wins_old = self.wins;
                self.losses_old = self.losses;
                self.probarr_old = self.probarr;
            }
        }
    }

    fn bot_move(&mut self, mut _board: &mut [char], turn: i8) {
        let mut this_move = -2;
        while !valid_move(this_move+1, &_board) {

            // Track total possibility total
            let mut prob = 0i64;

            // Add up all the odds
            for x in 0..9 {
                prob = prob + self.probarr[turn as usize][x] as i64;
            }

            // Take a random from 0 to prob total
            let mut random = rand::thread_rng().gen_range(1, prob);

            // For each possible move,
            for x in 0..9 {	
                // Subtract that moves probability from random,
                random = random - self.probarr[turn as usize][x] as i64;

                // Until random is less than 0, selecting that move
                if random <= 0 { 
                    // If it's a possible move, return it
                    this_move = x as i8; 
                    break; 
                }
            }
            if !valid_move(this_move+1, &_board) { this_move = 8; }
        }

        mark(this_move+1, self.play_char, &mut _board);

        self.history[turn as usize] = this_move;
    }

    fn name(&self) -> &str { "Learn" }
    fn c(&self) -> char { self.play_char }
}

impl Bot for HumanBot {
    fn bot_move(&mut self, mut _board: &mut [char], _turn: i8) {
		let mut s = -1;

		// Ask for user input
		while !mark(s, self.play_char, &mut _board)
		{
			print!("{}[8;1H", 27 as char);
			println!("Where goes {}?", self.play_char);
			let mut input_text = String::new();
			io::stdin()
				.read_line(&mut input_text)
				.expect("failed to read from stdin");

			let trimmed = input_text.trim();
			match trimmed.parse::<i8>() {
				Ok(i) => s = i,
				Err(..) => println!("Please enter a number 1-9"),
			};
		}
    }

    fn name(&self) -> &str { "Human" }
    fn c(&self) -> char { self.play_char }
}

impl Bot for FirstBot {
    fn bot_move(&mut self, mut _board: &mut [char], _turn: i8) {
        // For each space, in order
        for n in 1..10 {
            if mark(n, self.play_char, &mut _board){
                return;
            }
        }
    }

    fn name(&self) -> &str { "First" }
    fn c(&self) -> char { self.play_char }
    /* fn new(play_char: char) -> FirstBot {
        FirstBot {play_char: play_char}
    } */
}

// Mark first available odd, otherwise first available anywhere
impl Bot for OddFirstBot {
    fn bot_move(&mut self, mut _board: &mut [char], _turn: i8) {
	    for n in 1..10 {
	    	// If it's odd and valid, mark it
	    	if n % 2 != 0 && mark(n, self.play_char, &mut _board) {
	    		return;
	    	}
	    }
	    for n in 1..10 {
	    	// If it's even and valid, mark it
	    	if n % 2 == 0 && mark(n, self.play_char, &mut _board) {
	    		return;
	    	}
	    }
    }

    fn name(&self) -> &str { "OddFirst" }
    fn c(&self) -> char { self.play_char }
}

impl Bot for RandBot {
    fn bot_move(&mut self, mut _board: &mut [char], _turn: i8) {

	    let r = || rand::thread_rng().gen_range(1, 10);

	    // Until the move is valid,
	    while !mark(r(), self.play_char, &mut _board){ }
    }

    fn name(&self) -> &str { "Random" }
    fn c(&self) -> char { self.play_char }
}

fn bot_from_name(name: &str, _c: char) -> Box<dyn Bot> {
	match name {
		"odd" => 	Box::new(OddFirstBot{play_char: _c}),
		"rand" => 	Box::new(RandBot{play_char: _c}),
		"human" => 	Box::new(HumanBot{play_char: _c}),
		"learn" =>  Box::new(LearnBot {
                              play_char: _c, 
                              history: [-1; 9],
                              probarr: [[10000; 9]; 9], // Default odds set to 10000
                              probarr_old: [[10000; 9]; 9],
                              wins: 0,
                              losses: 0,
                              wins_old: -1,
                              losses_old: 0,
                              prob_iter: 0,
                             }),
		_ => Box::new(FirstBot{play_char: _c}),
	} 
}

fn main() {
    let mut botties: Vec<Box<dyn Bot>> = vec![];
	let mut fast = true; // false

	// Process arguments
	for argument in env::args() {
		let mut arg = argument.to_lowercase();

		if arg == "slow" { 
            fast = false; 
        }
		else if arg == "first" || arg == "rand" || arg == "random" || arg == "odd" 
             || arg == "learn" || arg == "human"{ 

			if arg == "random" { 
                arg = "rand".to_string(); 
            }

			if botties.len() == 0 { 
                botties.push(bot_from_name(&arg, 'X'));
            }
			else if botties.len() == 1 { 
                botties.push(bot_from_name(&arg, 'O'));
            }
            else if botties.len() == 3 {
                println!("More than 2 bots listed! Anything after first two will be ignored!");
                sleep(1);
            }
		}
	}

	if botties.len() == 2 {
		thread::spawn(move|| {
		let mut input: String = "yeet".to_string();	
			while input == "yeet".to_string() {
				io::stdin()
					.read_line(&mut input)
					.expect("failed to read from stdin");
			}
			std::process::exit(0);
		});
	}
	
	// Create the board array
	let mut board: [char; 9] = [' '; 9];

	// Run the intro sequence
	intro(&mut board);//       _______
    //                        |       |
    //                        | Hoot! |
    //                        |__  ___|                             
	// Init win tallies at 0     |/                                 |hoot.|
    //                         ,,,,                                    v
	let mut wins: [i32; 2] = [0 , 0]; let mut recent_wins: [i16; 2] = [0,0];
	let mut ties = 0;/*       (   )    //                 /   /       (   )
	                     ------m-m-------------------------------------m-m--------*///
    //                  _____________________________    _______________       _____
    //                                               \   \              \      \
	// Indefinite loop of games
	loop {
		// Set turn counter to 0
		let mut turn = 0;

		while game_over(&board) == ' '
		{
			// For each bot
			for b in 0..2 {
				// If game isn't over,
				if game_over(&board) == ' ' {
                    
                    botties[b].bot_move(&mut board, turn);
                    botties[0].learn(&board);
                    botties[1].learn(&board);

					// Pausing if not fast,
					if !fast || (wins[0] + wins[1]) % 500000 == 499999 { 
                        thread::sleep(::std::time::Duration::new(0, 900000000));
                    }

					turn = turn + 1;
                }
			}
		} // End of game while loop
        
        let winner = game_over(&board);
		if winner == 'P' { 
			ties = ties + 1; 
            let tie_percent = 100.0*(ties as f32/(wins[0] + wins[1]) as f32);
            print!("{}[5;16H  Ties: {} ({}%)    ", 27 as char, ties, tie_percent); 
		} else {
            for i in 0..2 {
                if winner == botties[i].c() {
                    wins[i] = wins[i] + 1;

                    let win_percent = 100.0*(wins[i] as f32/(wins[0]+wins[1]) as f32);
                    if wins[i] == 1 { 
                        print!("{}[{};16H  {} ({}) has {} win  ", 
                               27 as char, 2+i, botties[i].c(), botties[i].name(), wins[i]); 
                    }
                    else {
                        print!("{}[{};16H  {} ({}) has {} wins ({}%)", 
                               27 as char, 2+i, botties[i].c(), botties[i].name(), wins[i], win_percent); 
                    }

                    recent_wins[i] = recent_wins[i] + 1;
                }
            }
        }

		if botties.len() >= 2 {
            print!("{}[4;16H  Recent wins: {}%     ", 
                   27 as char, 100.0*(recent_wins[1] as f32/(recent_wins[1] + recent_wins[0]) as f32)); 

			// If the temp x win is above 500, 
            // reset both temp win variables
			if recent_wins[0] > 500 {
				recent_wins[0] = recent_wins[0] / 50;
				recent_wins[1] = recent_wins[1] / 50;
			}

			//// TEMP STATEMENT KILLING PROG AFTER 50000 ////
			//if wins[0] + wins[1] >= 50000 { return; }

			// Reset the board between games without clearing probability data
			print!("{}[1;1H\n", 27 as char);
			print!("    │   │  \n");
			print!(" ───┼───┼───\n");
			print!("    │   │  \n");
			print!(" ───┼───┼───\n");
			print!("    │   │   ");
		} 
		else {
			// If a player game is over, print_cat() for tie or announce the winner
			print!("{}[8;1H", 27 as char);
			if game_over(&board) != ' ' {
				if is_cat_game(&board) == 'P' {
					print_cat();
				} else {
					winner_graphic(); }
				//thread::sleep(::std::time::Duration::new(0, 900000000)); 
			}
		}

		// Reset board array
		for x in 0..9 { board[x] = ' '; }

	} // End of indefinite games loop
}

// Return true if given spot is open
fn valid_move(_spot: i8, _board: &[char]) -> bool
{
	// If the spot isn't a valid number, return false
	if _spot > 9 || _spot < 1 { return false; }

	// If the spot is empty, return true
	if _board[(_spot-1) as usize] == ' ' { return true; }
	
	// Otherwise return false
	else { return false; }
}

// Return victor char, or 'P' for a tie, if game is over, otherwise return ' '
fn game_over(_board: &[char]) -> char
{
	// Column-checker
	for x in 0..3 {
		if _board[x] == _board[x+3] && 
		   _board[x] == _board[x+6] && 
		   _board[x] != ' ' 
		{ return _board[x]; }
	}

	// Row-checker	
	for x in (0..7).step_by(3) {
		if _board[x] == _board[x+1] && 
		   _board[x] == _board[x+2] && 
		   _board[x] != ' ' 
		{ return _board[x]; }
	}

	// Top-left diagonal-checker
	if _board[0] == _board[4] && 
	   _board[0] == _board[8] && 
	   _board[0] != ' ' 
		{ return _board[0]; }

	// Top-right diagonal-checker
	if _board[2] == _board[4] && 
	   _board[2] == _board[6] && 
	   _board[2] != ' ' 
		{ return _board[2]; }

	return is_cat_game(&_board);
}

// Check for a full (tied) board
fn is_cat_game(_board: &[char]) -> char
{
	for x in 0..9 {
		if _board[x] == ' ' {
			return ' ';
		}
	}
	return 'P';
}

// Flashy intro sequence
fn intro(mut board: &mut [char])
{
	// Clear the screen, and print a blank board
	clear_screen();
	fast_string("\n".to_string());
	fast_string("    │   │  \n".to_string());
	fast_string(" ───┼───┼───\n".to_string());
	fast_string("    │   │  \n".to_string());
	fast_string(" ───┼───┼───\n".to_string());
	fast_string("    │   │  \n".to_string());
	fast_string("\n".to_string());

	// Set sleep time in ns
	let place_time = ::std::time::Duration::new(0, 100000000);

	// Serpentine 'X' animation
	for n in [1,4,7,8,5,2,3,6,9].iter() {
		mark(*n, 'X', &mut board);
		thread::sleep(place_time); 
	}

	// Flashing 'X' and 'O' animation
	for _x in 1..3 {
		for n in 1..10 { mark(n, 'O', &mut board); }
		thread::sleep(place_time*2); 

		for n in 1..10 { mark(n, 'X', &mut board); }
		thread::sleep(place_time*2); 
	}

	// Clear board
	for n in 1..10 {
		nocheck_mark(10-n, ' ', &mut board);
		thread::sleep(place_time); 
	}
}

// Mark the space on the terminal and in the board array
fn mark(_spot: i8, _c: char, _board: &mut [char]) -> bool
{
    if !valid_move(_spot, _board) {
        return false;
    }

    nocheck_mark(_spot, _c, _board);

    true
}

fn nocheck_mark(_spot: i8, _c: char, _board: &mut [char]) {
    let row = (_spot-1)/3 + 1;
    let col = _spot*4-(1 + 12*(row-1));
    print!("{}[{};{}H{}", 27 as char, row*2, col, _c);

	io::stdout().flush().expect("error in flush");
	print!("{}[16;16H", 27 as char);

	// Save given char to the board array	
	_board[(_spot-1) as usize] = _c;
}

// Animate a string by letter
fn fast_string(_s: String)
{
	for _c in _s.chars() {
		print!("{}", _c);
		io::stdout().flush().expect("error in flush");
		thread::sleep(::std::time::Duration::new(0, 3500000)); // The delay between each character
	}
}

fn sleep(_x: u64) {
    thread::sleep(::std::time::Duration::new(_x, 000000000));
}

// Clear terminal and move cursor to 1;1
fn clear_screen()
{
	// Escape sequence to clear terminal
	print!("{}[2J", 27 as char);

	// Escape sequence to jump to 1,1
	print!("{}[1;1H", 27 as char);
	io::stdout().flush().expect("error in flush");
}

// Print a cat for a cat (tie) game
fn print_cat()
{
    //let mut random = rand::thread_rng().gen_range(1, 2);
	//print!("{}[5;16H╔═══════════════════╗", 27 as char);
	//print!("{}[6;16H║ Play again, mrow? ║", 27 as char);
	//print!("{}[7;16H╚═══════════════════╝", 27 as char);
	print!("{}[11;16H              Play again, mrow?  ", 27 as char);
	print!("{}[12;16H                  |/             ", 27 as char);
	print!("{}[13;16HCat game!    /ᐠ ._. ᐟ\\ﾉ", 27 as char);
	//println!("cat game   （＾・ﻌ・＾✿）");
	//print!("{}[13;16H    Cat!   （^ ・ﻌ  ・^ ）", 27 as char);
	io::stdout().flush().expect("error in flush");
	print!("{}[13;1H ", 27 as char);
	println!("");
}

// Print a celebratory 'Tic-Tac-Toe'
fn winner_graphic()
{
	// Implementing a sparkling effect that covers a given rectangle of terminal space could be sweet
	fast_string(" _______        ______            ______         __\n".to_string());
	fast_string("/_  __(_)______/_  __/__ ________/_  __/__  ___ / /\n".to_string());
	fast_string(" / / / / __/___// / / _ `/ __/___// / / _ \\/ -_)_/\n".to_string());
	fast_string("/_/ /_/\\__/    /_/  \\_,_/\\__/    /_/  \\___/\\__(_)\n".to_string());
}

